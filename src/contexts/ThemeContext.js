import React, { Component, createContext } from 'react';

export const ContextProvider = createContext();

class App extends Component {
    state = {
        isLightTheme: true,
        light: { syntax: '#555', ui: '#ddd', bg: '#eee' },
        dark: { syntax: '#ddd', ui: "#333", bg: "#555" }
    }

    toggleTheme = () => {
        console.log("BRUh")
        this.setState({
            isLightTheme: !this.state.isLightTheme
        })
    }
    render() {
        return (
            <ContextProvider.Provider value={{ ...this.state, toggleTheme: this.toggleTheme }}>
                {this.props.children}
            </ContextProvider.Provider>
        );
    }
}

export default App;