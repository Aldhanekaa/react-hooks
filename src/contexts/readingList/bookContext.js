import React, { createContext, useState, useEffect } from 'react';

export const BookContext = createContext();

const BookContextProvider = props => {
    const [books, setBooks] = useState([])

    const addBook = (title, author) => {
        setBooks([...books, { title, author, id: new Date() }])
    }

    useEffect(() => {
        localStorage.setItem("shfudsiufhisdhfiushdiufhsdihfisdfishfisdifsdifh", JSON.stringify(books))
    }, [books])

    useEffect(() => {
        console.log(localStorage.getItem("shfudsiufhisdhfiushdiufhsdihfisdfishfisdifsdifh"))
    }, [])

    const removeBook = id => {
        setBooks(books.filter(book => book.id !== id));
    }

    return (
        <BookContext.Provider value={{ books, addBook, removeBook }}>
            {props.children}
        </BookContext.Provider>
    )
}

export default BookContextProvider;