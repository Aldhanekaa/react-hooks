import React, { Component } from 'react';
import { ContextProvider } from '../contexts/ThemeContext';


class ThemeToggle extends Component {
    static contextType = ContextProvider;

    render() {
        const { toggleTheme } = this.context;
        return (
            <button onClick={toggleTheme}>Change!</button>
        );
    }
}

export default ThemeToggle;