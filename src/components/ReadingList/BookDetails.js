import React, { useContext } from 'react';
import { BookContext } from '../../contexts/readingList/bookContext';
const BookDetails = ({ book }) => {
    const { removeBook } = useContext(BookContext);

    return (
        <li>
            <div class="title">{book.title}</div>
            <div class="author">{book.author}</div>
            <button onClick={() => removeBook(book.id)}>Remove </button>
        </li>
    )
}

export default BookDetails;