import React, { useState, useContext } from 'react';
import { BookContext } from '../../contexts/readingList/bookContext';

const App = () => {
    const { addBook } = useContext(BookContext);
    const [title, setTitle] = useState();
    const [author, setAuthor] = useState();

    const onSubmit = event => {
        event.preventDefault();

        addBook(title, author)

        setTitle('');
        setAuthor('');
    }

    return (
        <form onSubmit={onSubmit}>
            <li><label>Book title:</label><input type="text" value={title} id="title" onChange={(e) => setTitle(e.target.value)} /></li>
            <li><label>Book author:</label><input type="text" id="author" value={author} onChange={(e) => setAuthor(e.target.value)} /></li>
            <li><input type="submit" value="Add Book" /></li>
        </form>
    )
}

export default App;