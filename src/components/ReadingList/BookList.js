import React, { useContext } from 'react';
import { BookContext } from '../../contexts/readingList/bookContext';
import BookDetails from './BookDetails';

const BookList = () => {
    const { books } = useContext(BookContext);
    return books.length ? (
        <div class="book-list">
            <ul>
                {books.map(book => (
                    <BookDetails book={book} key={book.id} />
                ))}
            </ul>
        </div>
    ) : <div class="empty">No books to read. Hello free time :)</div>
}

export default BookList;