import Navbar from "./Navbar/index";
import Home from "../containers/Home";
import Discussion from "../containers/Discussion";
import ThemeContextProvider from '../contexts/ThemeContext'
import AuthContextProvider from '../contexts/authContext';
import SongList from './songList';
import ReadingList from '../containers/ReadingList';

import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";

export default function App() {
  return (
    <Router>
      <Switch>

        <Route exact path="/reading-list">
          <ReadingList />
        </Route>

        <Route exact path="/song-list">
          <SongList />
        </Route>

        <Route exact path="/">
          <ThemeContextProvider>
            <AuthContextProvider>
              <Navbar />
              <Home />
            </AuthContextProvider>
          </ThemeContextProvider>
        </Route>

        <Route >
          <NotFound />
        </Route>

      </Switch>
    </Router >
  );
}

function NotFound() {
  return (
    <p>Error Not Found</p>
  )
}