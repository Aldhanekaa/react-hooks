import { Link } from "react-router-dom";
import React, { Component } from 'react';
import { ContextProvider } from '../../contexts/ThemeContext'
import { AuthContext } from '../../contexts/authContext'

/*
class Navbar extends Component {
  // static contextType = ContextProvider;
  render() {
    // const { isLightTheme, light, dark } = this.context;
    // const theme = isLightTheme ? light : dark;
    return (
      <ContextProvider.Consumer>{context => {
        const { isLightTheme, light, dark } = context;
        const theme = isLightTheme ? light : dark;
        return (
          <nav style={{ background: theme.ui, color: theme.syntax }}>
            <h1>Context App</h1>
            <ul>
              <li>Home</li>
              <li>About</li>
              <li>Contact</li>
            </ul>
          </nav>
        )
      }}

      </ContextProvider.Consumer>
    );
  }
}
*/

const Navbar = props => {
  return (
    <AuthContext.Consumer>
      {authContext => (
        <ContextProvider.Consumer>{themeContext => {
          const { isAuthenticated, toggleAuth } = authContext;
          const { isLightTheme, light, dark } = themeContext;
          const theme = isLightTheme ? light : dark;
          return (
            <nav style={{ background: theme.ui, color: theme.syntax }}>
              <h1>Context App</h1>
              <div onClick={toggleAuth}>
                {isAuthenticated ? 'logged in' : 'logged out'}
              </div>
              <ul>
                <li>Home</li>
                <li>About</li>
                <li>Contact</li>
              </ul>
            </nav>
          )
        }}

        </ContextProvider.Consumer>
      )}
    </AuthContext.Consumer>
  )
}

export default Navbar;
