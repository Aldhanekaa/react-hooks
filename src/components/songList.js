import React, { Component, useState, useEffect } from 'react';
import NewSongForm from './newSongForm';

const SongList = () => {
    const [songs, setSongs] = useState([
        { title: 'almost home', id: 1 },
        { title: 'memory gaspel', id: 2 },
        { title: 'this wild darkness', id: 3 }

    ])

    const addSong = title => {
        setSongs([...songs, { title, id: Math.random() }])
    }

    return (
        <div className="song-list">
            <ul>
                {songs.map(song => (
                    <li key={song.id}>{song.title}</li>
                ))}
            </ul>
            <NewSongForm addSong={addSong} />
        </div>
    )
}

export default SongList;