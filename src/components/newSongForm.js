import React, { useState } from 'react';

const NewSongForm = props => {
    const [title, setTitle] = useState('');
    const handleSubmit = event => {
        event.preventDefault();
        console.log(title);
        props.addSong(title);
    }
    return (
        <form action="" onSubmit={handleSubmit}>
            <label >song name:</label>
            <input type="text" required onChange={e => setTitle(e.target.value)} />
            <input type="submit" value="add song" />
        </form>
    )
}

export default NewSongForm;