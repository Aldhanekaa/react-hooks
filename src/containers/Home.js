import BookList from "../components/BookList";
// import ContextProvider from '../contexts/ThemeContext';
import ToggleTheme from '../components/ThemeToggle';

const Home = () => {
  return (
    <>
      <BookList />
      <ToggleTheme />
    </>
  );
};

export default Home;
