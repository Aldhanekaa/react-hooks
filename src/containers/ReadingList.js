import React, { Component } from 'react';
import BookContextProvider from '../contexts/readingList/bookContext';
import Navbar from './wth';
import BookList from '../components/ReadingList/BookList';
import BookForm from '../components/ReadingList/BookForm';

class App extends Component {
    render() {
        return (
            <BookContextProvider >
                <Navbar />
                <BookList />
                <BookForm />
            </BookContextProvider>
        )
    }
}

export default App;